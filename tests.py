import unittest
import os
import sqlite3

from knexPy import knex_connect

class TestSELECT(unittest.TestCase):
  def tearDown(self):
    os.remove('example.db')

  def test_select(self):
    database_connection = sqlite3.connect('example.db')
    knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'example.db'}})

    database_connection.execute('CREATE TABLE test_table (col_1 INT, col_2 TEXT)')
    database_connection.commit()
    self.assertEqual(knex('test_table').select().then(), [])
    self.assertEqual(knex('test_table').select('*').then(), [])
    self.assertEqual(knex('test_table').select('col_1').then(), [])
    # self.assertEqual(knex('test_table').select('bad_column_name').then(), [])

    database_connection.execute('INSERT INTO test_table (col_1, col_2) VALUES (5, "hi there")')
    database_connection.commit()
    self.assertEqual(knex('test_table').select().then(), [{'col_1': 5, 'col_2': 'hi there'}])
    self.assertEqual(knex('test_table').select('*').then(), [{'col_1': 5, 'col_2': 'hi there'}])
    self.assertEqual(knex('test_table').select('col_1').then(), [{'col_1': 5}])

    database_connection.execute('INSERT INTO test_table (col_1, col_2) VALUES (6, "hi there again")')
    database_connection.commit()
    self.assertEqual(knex('test_table').select().then(), [{'col_1': 5, 'col_2': 'hi there'}, {'col_1': 6, 'col_2': 'hi there again'}])
    self.assertEqual(knex('test_table').select('*').then(), [{'col_1': 5, 'col_2': 'hi there'}, {'col_1': 6, 'col_2': 'hi there again'}])
    self.assertEqual(knex('test_table').select('col_2').then(), [{'col_2': 'hi there'}, {'col_2': 'hi there again'}])

  def test_select_join(self):
    database_connection = sqlite3.connect('example.db')
    knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'example.db'}})

    database_connection.execute('CREATE TABLE tt1 (col_1 INT, col_2 TEXT)')
    database_connection.execute('CREATE TABLE tt2 (col_1 INT, col_2 REAL)')
    database_connection.commit()
    self.assertEqual(knex('tt1').select().join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [])
    self.assertEqual(knex('tt1').select('*').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [])
    self.assertEqual(knex('tt1').select('tt1.col_1').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select().then(), [])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('*').then(), [])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('tt2.col_1').then(), [])

    database_connection.execute('INSERT INTO tt1 (col_1, col_2) VALUES (4, "hi there")')
    database_connection.execute('INSERT INTO tt2 (col_1, col_2) VALUES (5, 5.5)')
    database_connection.commit()
    self.assertEqual(knex('tt1').select().join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}])
    self.assertEqual(knex('tt1').select('*').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}])
    self.assertEqual(knex('tt1').select('tt1.col_1').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select().then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('*').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('tt2.col_2').then(), [{'tt2.col_2': 5.5}])

    database_connection.execute('INSERT INTO tt1 (col_1, col_2) VALUES (6, "hi there again")')
    database_connection.execute('INSERT INTO tt2 (col_1, col_2) VALUES (7, 7.7)')
    database_connection.commit()
    self.assertEqual(knex('tt1').select().join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}, {'tt1.col_1': 6, 'tt1.col_2': 'hi there again', 'tt2.col_1': 7, 'tt2.col_2': 7.7}])
    self.assertEqual(knex('tt1').select('*').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}, {'tt1.col_1': 6, 'tt1.col_2': 'hi there again', 'tt2.col_1': 7, 'tt2.col_2': 7.7}])
    self.assertEqual(knex('tt1').select('tt1.col_1').join('tt2', 'tt1.rowid', 'tt2.rowid').then(), [{'tt1.col_1': 4}, {'tt1.col_1': 6}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select().then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}, {'tt1.col_1': 6, 'tt1.col_2': 'hi there again', 'tt2.col_1': 7, 'tt2.col_2': 7.7}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('*').then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}, {'tt1.col_1': 6, 'tt1.col_2': 'hi there again', 'tt2.col_1': 7, 'tt2.col_2': 7.7}])
    self.assertEqual(knex('tt1').join('tt2', 'tt1.rowid', 'tt2.rowid').select('tt2.col_2').then(), [{'tt2.col_2': 5.5}, {'tt2.col_2': 7.7}])

  def test_select_where_andwhere(self):
    database_connection = sqlite3.connect('example.db')
    knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'example.db'}})

    database_connection.execute('CREATE TABLE tt (col_1 INT, col_2 TEXT)')
    database_connection.commit()

    for where_func in (knex.where, knex.andwhere):
      self.assertEqual(
          where_func(knex('tt').select(), *('rowid', 1)).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), *('col_1', 4)).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('col_1'), *('col_2', '<>', 'hi there')).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1})).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4})).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1, 'col_2': 'never there'})).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4, 'col_2': 'hi there'})).then(),
          []
        )

      database_connection.execute('INSERT INTO tt (col_1, col_2) VALUES (4, "hi there")')
      database_connection.commit()
      self.assertEqual(
          where_func(knex('tt').select(), *('rowid', 1)).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), *('col_1', 4)).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('col_1'), *('col_2', '<>', 'hi there')).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1, 'col_2': 'never there'})).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4, 'col_2': 'hi there'})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )

      database_connection.execute('INSERT INTO tt (col_1, col_2) VALUES (5, "bye")')
      database_connection.commit()
      self.assertEqual(
          where_func(knex('tt').select(), *('rowid', 1)).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), *('col_1', 4)).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('col_1'), *('col_2', '<>', 'hi there')).then(),
          [{'col_1': 5}]
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          where_func(knex('tt').select(), ({'rowid': 1, 'col_2': 'never there'})).then(),
          []
        )
      self.assertEqual(
          where_func(knex('tt').select('*'), ({'col_1': 4, 'col_2': 'hi there'})).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )
      self.assertEqual(
          knex('tt').select().where('rowid', 1).where({'col_1': 4}).then(),
          [{'col_1': 4, 'col_2': "hi there"}]
        )

      database_connection.execute('DELETE FROM tt')
      database_connection.commit()

  def test_select_join_where_frm(self):
    database_connection = sqlite3.connect('example.db')
    knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'example.db'}})

    database_connection.execute('CREATE TABLE tt1 (col_1 INT, col_2 TEXT)')
    database_connection.execute('CREATE TABLE tt2 (col_1 INT, col_2 REAL)')
    database_connection.execute('INSERT INTO tt1 (col_1, col_2) VALUES (4, "hi there")')
    database_connection.execute('INSERT INTO tt2 (col_1, col_2) VALUES (5, 5.5)')
    database_connection.commit()
    self.assertEqual(knex().select().frm('tt1').join('tt2', {'tt1.rowid': 'tt2.rowid'}).where('tt1.col_1', 4).then(), [{'tt1.col_1': 4, 'tt1.col_2': 'hi there', 'tt2.col_1': 5, 'tt2.col_2': 5.5}])
    # could be more tests if errors start to arise that are not caught in test_select_join and test_select_where

# class TestUPDATE(unittest.TestCase):
#   def tearDown(self):
#     os.remove('example.db')


if __name__ == '__main__': unittest.main()
