import re
from itertools import chain


def knex_connect(settings):
  '''
  Requires: dictionary containing {'client': NAME OF DATABASE HANDLER, 'connection': {CONNECTION SETTINGS}}
  Effects:  returns a knex sql database wrapper and sql statement generator tools
  Note:     Only insert, update, select, join,
  ex:
  knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'knex_test.db'}})
  knex().frm('wax').insert({'col1': 'I\'m a free', 'col2': 'boss'}).then()
  query_data = knex('wax').select('col1', 'col2').where('col1', 'free').then()
  '''
  if settings['client'] == 'sqlite3':
    import sqlite3

  class Schema:
    def __init__(self):
      self.query_type = None
      self.table = None
      self.columns = None

    def createTable(self, tableName, column_creator):
      self.query_type = 'CREATE TABLE'
      self.table = table(tableName)
      column_creator(self.table)

    def _compile_sql_statement(self):
      insert_data = []

      if self.query_type == 'CREATE TABLE':
        stm = [
            'CREATE TABLE', '?',
            '(' + ', '.join('? ' + col['type'] for col in self.table.columns) + ')'
          ]
        insert_data.append(self.table.name)
        insert_data += (col_name for col_name in self.table.columns)

      else: raise NotImplementedError('SQL command "' + self.query_type + '" not implemented yet.')

      return sql_statement, insert_data


    def then(self, callback=None):
      if settings['client'] == 'sqlite3':
        database_connection = sqlite3.connect(settings['connection']['filename'])

        sql_statement = self._compile_sql_statement()
        if self.query_type == 'CREATE TABLE':
          try:
            database_connection.execute(*sql_statement)
          except:
            if callback: callback(False)
            else: return False

        else: raise NotImplementedError

        database_connection.commit()

        if callback: callback(True)
        else: return True

      else: raise NotImplementedError('This operation is not implemented for "' + settings['client'])


  class knex:
    # client = settings['client']
    # connection = settings['connection']
    schema = Schema()

    def __init__(self, table=None):
      self.table = table

      self.query_type = None  #  select, insert, update, delete...
      self.columns = None

      self.joins = []
      self.wheres = None

    # from
    def frm(self, table):
      self.table = table
      return self

    def select(self, *columns):
      self._set_query_type('SELECT')

      if len(columns) == 0:
        columns = ('*',)
      if len(columns) == 1 and isinstance(columns[0], (list, tuple)):
        columns = columns[0]

      if self.columns is None:
        self.columns = []
      self.columns += columns

      return self

    def update(self, *args):
      self._set_query_type('UPDATE')

      if len(args) == 1 and isinstance(args[0], dict):
        self.columns = args[0]
      else:
        raise NotImplementedError

      return self

    def insert(self, *args):
      self._set_query_type('INSERT')

      if len(args) == 1 and isinstance(args[0], dict):
        self.columns = args[0]
      else:
        raise NotImplementedError

      return self

    def delete(self):
      self._set_query_type('DELETE')
      return self

    def where(self, *args):
      if self.wheres is None:
        self.wheres = []

      if len(args) == 1 and isinstance(args[0], dict):
        cur_self = self
        for key, value in args[0].items():
          cur_self = cur_self.where(key, value)
          # self.wheres.append({'left': key, 'right': args[0][key], 'operator': '=', 'combiner': 'AND'})
        return cur_self


      elif len(args) == 2:
        return self.where(args[0], '=', args[1])
        # self.wheres.append({'left': args[0], 'right': args[1], 'operator': '=', 'combiner': 'AND'})

      elif len(args) == 3:
        params = {'left': args[0], 'right': args[2], 'operator': args[1], 'combiner': 'AND'}
        if params['right'] is None: params['operator'] = ' is '
        self.wheres.append(params)

      else:
        raise WrongArguments(args)

      return self

    def _set_query_type(self, q_type):
      if self.query_type is None:
        self.query_type = q_type
      elif self.query_type != q_type:
        raise ConflictingQueryType('Query type already set')

    andwhere = where

    def join(self, table, *args):
      if len(args) == 1 and isinstance(args[0], dict):
        self.joins.append({'table': table, 'operator': '=', 'combiner': 'AND', 'on': args[0]})
      elif len(args) == 2:
        self.joins.append({'table': table, 'operator': '=', 'combiner': 'AND', 'on': {args[0]: args[1]}})
      elif len(args) == 3:
        self.joins.append({'table': table, 'operator': args[1], 'combiner': 'AND', 'on': {args[0]: args[2]}})
      else:
        raise WrongArguments

      return self

    def _compile_sql_statement(self, safeties_on=False):
      insert_data = []

      if self.query_type == 'SELECT':
        sql_statement = 'SELECT '

        if len(self.columns) == 1 and self.columns[0] == '*':
          database_connection = sqlite3.connect(settings['connection']['filename'])
          if self.joins:
            self.columns = []
            for table in chain((self.table,), (j['table'] for j in self.joins)):
              self.columns += (table + '.' + column_info[1] for column_info in database_connection.execute('pragma table_info('+table+')').fetchall())
          else:
            self.columns = tuple(column_info[1] for column_info in database_connection.execute('pragma table_info('+self.table+')').fetchall())

        for i in range(len(self.columns)):
          sql_statement +=  _quotes_dot(self.columns[i])
          if i != len(self.columns) - 1:
            sql_statement += ','
          sql_statement += ' '

        sql_statement += 'FROM ' + _quotes_dot(self.table) + ' '

      elif self.query_type == 'UPDATE':
        sql_statement = 'UPDATE ' + _quotes_dot(self.table) + ' SET '

        left = tuple(k for k in dict.keys(self.columns))
        for key in left:
          sql_statement +=  _quotes_dot(key) + '=?'
          insert_data.append(self.columns[key])
          if key != left[-1]:
            sql_statement += ','
          sql_statement += ' '

      elif self.query_type == 'INSERT':
        sql_statement = 'INSERT INTO ' + _quotes_dot(self.table) + ' ('

        columns = tuple(col for col in dict.keys(self.columns))
        for col in columns:
          sql_statement +=  _quotes_dot(col)
          if col != columns[-1]:
            sql_statement += ', '

        sql_statement += ') VALUES ('

        for col in columns:
          sql_statement += '?'
          insert_data.append(self.columns[col])
          if col != columns[-1]:
            sql_statement += ', '

        sql_statement += ') '

      elif self.query_type == 'DELETE':
        sql_statement = 'DELETE FROM ' + _quotes_dot(self.table)

        if not self.wheres and safeties_on:
          raise VerifyBadHabit('No WHERE defined for DELETE query.')

      else:
        raise NotImplementedError

      if self.joins:
        for jn in self.joins:
          sql_statement += 'JOIN ' + _quotes_dot(jn['table']) + ' ON '
          left = tuple(k for k in dict.keys(jn['on']))
          for key in left:
            if key != left[0]:
              sql_statement += jn['combiner'] + ' '

            sql_statement +=  _quotes_dot(key) + jn['operator'] + _quotes_dot(jn['on'][key]) + ' '

      if self.wheres:
        sql_statement += 'WHERE '

        for i in range(len(self.wheres)):
          if i != 0:
            sql_statement += self.wheres[i]['combiner'] + ' '
          sql_statement += _quotes_dot(self.wheres[i]['left']) + self.wheres[i]['operator'] + '? '
          insert_data.append(self.wheres[i]['right'])

      return sql_statement, insert_data

    # execute the sql that has been built
    def then(self, callback=None):
      # if self.client == 'sqlite3':
      if settings['client'] == 'sqlite3':
        # database_connection = sqlite3.connect(self.connection['filename'])
        database_connection = sqlite3.connect(settings['connection']['filename'])

        if self.query_type == 'SELECT':
          rows = database_connection.execute(*self._compile_sql_statement()).fetchall()
          for i in range(len(rows)):
            row = rows[i]
            rows[i] = {}
            for i2 in range(len(self.columns)):
              rows[i][self.columns[i2]] = row[i2]

          if callback:
            callback(rows)
          else:
            return rows

        elif self.query_type in ['UPDATE', 'INSERT', 'DELETE']:
          # this attempts to fix unsuppoort data types by converting them into strings
          # this method should probably also be applied to the select method
          count = 0
          not_finished = True
          sql_statement = self._compile_sql_statement()
          while not_finished and count < len(sql_statement[1]):
            try:
              database_connection.execute(*sql_statement)
              not_finished = False
            except sqlite3.InterfaceError as error:
              parameter = int(re.search(r'\d+', error.args[0]).group())
              sql_statement[1][parameter] = str(sql_statement[1][parameter])
            count += 1
          database_connection.commit()

          if callback: callback(True)
          else: return True

        else: raise NotImplementedError
      else: raise NotImplementedError



    # execute a direct sql statment just like the sqlite3.execute ffunction
    # def execute(*args):

  return knex


class table:
  def __init__(self, name):
    self.name = name
    self.columns = []

  def integer(self, name):
    self.columns.append({'name': name, 'type': 'INTEGER'})

  def real(self, name):
    self.columns.append({'name': name, 'type': 'REAL'})

  def text(self, name):
    self.columns.append({'name': name, 'type': 'TEXT'})

  def blob(self, name):
    self.columns.append({'name': name, 'type': 'BLOB'})


# returns the string with quotes around all parts that are seperated by periods "."
def _quotes_dot(needs_quotes):
  needs_quotes = needs_quotes.split('.')
  out_string = ''
  for i in range(len(needs_quotes)):
    out_string += '"' + needs_quotes[i] + '"'
    if i < len(needs_quotes) - 1:
      out_string += '.'

  return out_string

class ConflictingQueryType(BaseException): pass
class WrongArguments(BaseException): pass
class VerifyBadHabit(BaseException): pass

