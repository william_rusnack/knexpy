# knexPy NO LONGER SUPPORTING THIS REPO
A query builder for SQLite3 (soon to be more), designed to be flexible, portable, and fun to use.
![](https://github.com/BebeSparkelSparkel/knexPy/blob/master/SQL.jpg?raw=true)

Designed to be a python equivilent of the knex.js

How to import knex.
```python
from knexPy import knex_connect
knex = knex_connect({'client': 'sqlite3', 'connection': {'filename': 'path/to/sqlite/file'}})
```

How to use knex
```python
results = knex('table_name'  # can also be knex().from('table_name')
).select('column1', 'column2', ...
).where('column_val', 5  # same as .where('column_val', '=', 5) and .where({'column_val': 5})
).then()
```
How to install with pip
```shell
pip install --upgrade git+git://github.com/BebeSparkelSparkel/knexPy.git
pip3 install --upgrade git+git://github.com/BebeSparkelSparkel/knexPy.git 
pip3.X install --upgrade git+git://github.com/BebeSparkelSparkel/knexPy.git
```

## Implemented Operations
#### Select
```python
.select() #selects all columns
.select(['column_name1', 'column_name2'])  # as a tuple or list
.select('column_name1', 'column_name2')
```

#### Update
```python
.update({'column_name1': new_value, 'column_name2': new_value})
```

#### Delete
```python
.delete()  # should be used with a should specify with .where command
```

#### Where and andwhere
```python
.where('column_name', specified_value)  # gives "WHERE column_name='specified_value'"
.where('column_name', '>', specified_value)  # gives "WHERE column_name>'specified_value'"
.where({'column_name': specified_value, 'column_name2': specified_value2})  # gives "WHERE column_name='specified_value' AND column_name2='specified_value2'"
```

#### Join
`.join(table, first, [operator], second)`

```python
knex('users'
).join('contacts', 'users.id', '=', 'contacts.user_id'
).select('users.id', 'contacts.phone')
```
Outputs: `select 'users'.'id', 'contacts'.'phone' from 'users' inner join 'contacts' on 'users'.'id' = 'contacts'.'user_id'`

```python
knex('users'
).join('contacts', 'users.id', 'contacts.user_id'
).select('users.id', 'contacts.phone')
```
Outputs: `select 'users'.'id', 'contacts'.'phone' from 'users' inner join 'contacts' on 'users'.'id' = 'contacts'.'user_id'`

## Returned Queries
To get the results of a query use .then()
It returns a list of dictionaries with the column names as the key and associated value for the row.
```python
>>> knex('names').select('first', 'last').then()  # returns the result
({'first': 'Bob', 'last': 'Smith'}, {'first': 'Jack', 'last': 'Adams'})
```

## Adding More
I'm currently adding more operations as I need them. If you feel like contributing to this project please do. When adding functions I am trying to copy the syntax from [knexjs.org](http://knexjs.org/) as much as possible.
