from setuptools import setup, find_packages

setup(name='knexPy',
      version='0.0',
      license='MIT',
      description='A query builder designed to be flexible, portable, and fun to use',
      author='William Rusnack',
      author_email='williamrusnack@gmail.com',
      url='https://github.com/BebeSparkelSparkel/knexPy/tree/master',
      classifiers=['Development Status :: 2 - Pre-Alpha', 'Programming Language :: Python :: 3'],
      # packages=find_packages(),
      # packages=['knexPy'],
      py_modules=["knexPy"],
     )
